const SHA = require('crypto-js/sha256');
const moment = require('moment');
const app = require('F:/Internship/project_final/app')
const whilst = require('async/whilst');

class Block {

    constructor(index, ts, data, prehash = '') {
        if (ts === undefined) {

            this.nonce = 0;
            this.index = index;
            this.ts = ts;
            this.data = data;

            this.hash = this.calcHash();

        }
        else {
            this.index = index;
            this.ts = ts;
            this.data = data;
            this.prehash = prehash;
            this.hash = this.calcHash();
            this.nonce = 0;
        }

    }


    calcHash() {

        return SHA(this.index + this.ts + this.prehash + JSON.stringify(this.data) + this.nonce).toString();

    }

    mineBlock(diff, bc, io) {
        let othis= this;
        
        whilst(
            function () { return (othis.hash.substring(0, diff) !== Array(diff + 1).join("0")); },
            function (callback) {
               
              
                othis.nonce++;
                othis.hash = othis.calcHash();
                setTimeout(function () {
                    callback(null, null);
                }, 0);

            },
            function (err, n) {
               
                
                bc.chain.push(othis);
                io.sockets.emit('block_added', { block: JSON.stringify(bc), ltb: JSON.stringify(bc.getLatestBlock()), tims: bc.getTimeStamp() })

            }
        );
    }

    mineBlockOld(diff) {

        while (this.hash.substring(0, diff) !== Array(diff + 1).join("0")) {
            console.log(this.nonce)
            this.nonce++;
            this.hash = this.calcHash();



        }
        this.ts = moment().format('MMMM Do YYYY, h:mm:ss a');

    }
}

module.exports = Block;