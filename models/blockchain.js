let Block = require('./block');

class Blockchain {


    constructor() {
        this.chain = [this.createGenesis()];

    }

    createGenesis() {
        return new Block(0, "03/01/2009", "Genesis-Block", "0");
    }

    getLatestBlock() {
        return this.chain[this.chain.length - 1];
    }
    getTimeStamp() {
        return this.getLatestBlock().ts;
    }



    addBlock(newBlock, io) {


        newBlock.prehash = this.getLatestBlock().hash;
        newBlock.mineBlock(2, this, io);



    }




}
module.exports = Blockchain;