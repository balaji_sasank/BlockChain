const express = require('express')
const whilst = require('async/whilst');
var app = express();
var session = require('express-session')
var count = 0;
var bp = require('body-parser');
app.set('views', './views');
app.set('view engine', 'ejs');
var users = ['Balaji', 'Lekha', 'Shiva', 'John'];
app.use(express.static('F:/Internship/project_final/assets'))
app.use(bp());
let Block = require('./models/block');
let BlockChain = require('./models/blockchain')
var uid, inf
var index = 0;
var ses =
    {
        secret: "abcdefg",
        saveUninitialized: false,
        resave: false
    }
app.use(session(ses))

app.get('/', (req, res) => {
    ses = req.session;
    res.render('login', { dis: "" });

});

app.post('/', (req, res) => {
    ses = req.session;
    ses.uid = req.body.id;

    if (users.includes(ses.uid)) {

        res.redirect('/main');
    }
    else {
        console.log("user does not exist")
        res.render('login', { dis: "User doesn't exist" });
    }
})

app.get('/main', (req, res) => {
    ses = req.session
    res.render('main', { unm: ses.uid })
})
var bcnet = new BlockChain();
server = app.listen(7900, () => console.log('Listening on port 7900....'));
const io = require('socket.io')(server)
app.post('/main', (req, res) => {
    ses = req.session;
    inf = req.body.ab;
    bcnet.addBlock(new Block(++index, undefined, inf), io)
    //  io.sockets.emit('block_added', { block: JSON.stringify(bcnet), ltb: JSON.stringify(bcnet.getLatestBlock()), tims: bcnet.getTimeStamp() })
    res.render('main', { unm: ses.uid })
})


io.on('connection', (socket) => {

    ++count;
    io.sockets.emit('users_connected', { uc: count });
        socket.on('disconnect', () => {
            count--;
             io.sockets.emit('users_disconnected', { uc: count });

        });
     io.sockets.emit('block', { block: JSON.stringify(bcnet), ltbt: JSON.stringify(bcnet.getLatestBlock()), timst: bcnet.getTimeStamp() })
})

app.get("/test", function (req, res, next) {
    ses = req.session
    var count = 0;
    whilst(
        function () { return count < 100; },
        function (callback) {
            console.log("count--"+count)
            count++;
            // callback(null, count);
             setTimeout(function() {
                callback(null, count);  
              }, 0);

        },
        function (err, n) {
            // 5 seconds have passed, n = 5
            console.log("n--" + n);
        }
    );
    console.log("thread freed")
    res.render('main', { unm: ses.uid })
})



